# Frogsolver <!-- omit int toc -->  

- [Frogsolver](#frogsolver)
  - [Description](#description)
  - [Rules](#rules)
  - [Imput format](#imput-format)
  - [Authors](#authors)

## Description  

A solver for the game Hoppers invented by Nob Yoshigahara.

## Rules  

1. Frog can move from lily pad in all the 8 directions.
2. You may only move by jumping over another frog on an adjacent lily pad,
   landing on the next pad.
3. When a frog is jumped over, remove it from the pond.
4. No non-jump moves are allowed. Frogs may not jump over an empty lily pad.
   No frog may land on another frog. No frog may jump over two frogs at a time.
5. When only one frog remains on the pond, YOU WIN!

## Imput format  

An input data set is a plain text file containing only ASCII characters
with lines ending with a single '\n' character (also called
“UNIX-style” line endings).
When multiple numbers are given in one line, they are separated
by a single space between each two numbers.

- The first line contains 2 numbers:
  - An integer *$c$* - The number of columns.
  - An integer *$l$* - The number of lines.
- The next *$l$* lines contains descriptions of lines of lily pad.
  Each entry can be:
  - A '0' representing an empty lily pad.
  - A '1' representing a lily pad with a frog on it.
  - A 'x' representing an empty place. There is no lily pad, nor frog.
    Lily pad from each sides of an empty places are considered adjacent.


## Authors  

- [ZERBIB Timothée](https://gitlab.com/Franz.Hopper)