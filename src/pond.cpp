#include <cstdlib>
#include <iostream>
#include "pond.h"


Pond::Pond(size_t l, size_t c, size_t f, char* g)
: l(l), c(c), nb_frogs(f), grid(g){}


Pond::Pond(const Pond& p){
  this->l = p.get_l();
  this->c = p.get_c();
  this->nb_frogs = p.getNbFrogs();

  // allocate the grid
  this->grid = new char[this->l * this->c];
  if(this->grid == NULL){
    std::cerr << "ERROR: while allocating the pond grid" << std::endl;
    exit(EXIT_FAILURE);
  }

  // copy the grid content
  this->copyGrid(p.grid);
}


Pond::~Pond(){
  delete[] this->grid;
}


Pond& Pond::operator=(const Pond& p){
  // self assignment check
  if(this != &p){
    // deallocation
    free(this->grid);

    // copy
    this->l = p.get_l();
    this->c = p.get_c();
    this->nb_frogs = p.getNbFrogs();

    // allocate then copy the grid
    this->grid = new char[this->l * this->c];
    if(this->grid == NULL){
      std::cerr << "ERROR: while allocating the pond grid" << std::endl;
      exit(EXIT_FAILURE);
    }
    this->copyGrid(p.getGrid());
  }

  // allow assignment sequences
  return *this;
}


size_t Pond::get_l() const{
  return this->l;
}


size_t Pond::get_c() const{
  return this->c;
}


size_t Pond::getNbFrogs() const{
  return this->nb_frogs;
}


size_t Pond::setNbFrogs(size_t n){
  return this->nb_frogs = n;
}


char* Pond::getGrid() const{
  return this->grid;
}


// assert that this->grid is already allocated
// assert that g has at least this->l lines and this->c columns
void Pond::copyGrid(char* g){
  // check if g is null
  if(!g){
    std::cerr << "ERROR: copy grid from null pointer" << std::endl;
    exit(EXIT_FAILURE);
  }

  // copy the grid content
  for(size_t i=0; i<this->l; ++i){
    for(size_t j=0; j<this->c; ++j){
      this->grid[i*this->c+j] = g[i*this->c+j];
    }
  }
}


void Pond::printGrid() const{
  char c;

  for(size_t i=0; i<this->l; ++i){
    for(size_t j=0; j<this->c; ++j){
      c = this->grid[i*this->c+j];
      if(c == EMPTY)
        std::cout << "- ";
      else if(c == NOFROG)
        std::cout << "0 ";
      else
        std::cout << "f ";
    }
    std::cout << std::endl;
  }
}


void Pond::addFrog(size_t l, size_t c){
  this->grid[l*this->c + c] = FROG;
  this->nb_frogs++;
}

void Pond::delFrog(size_t l, size_t c){
  this->grid[l*this->c + c] = NOFROG;
  this->nb_frogs--;
}
