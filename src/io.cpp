#include <iostream>
#include <fstream>
#include <cstdlib>
#include "io.h"


Pond* read_grid(std::string filename){
  std::ifstream f;
  f.open(filename, std::fstream::in);

  // Check for error
  if(!f.is_open()){
    std::cerr << "ERROR: File " << filename << " can't be open." << std::endl;
    exit(EXIT_FAILURE);
  }

  // Read number of lines and columns
  size_t l, c;
  size_t max_frogs, frogs=0;
  f >> l >> c;
  f >> max_frogs;

  // allocate the grid
  char* grid = new char[l * c];
  if(!grid){
    std::cerr << "ERROR: while allocating the pond grid" << std::endl;
    exit(EXIT_FAILURE);
  }

  char r;
  for(size_t i=0; i<l*c; ++i){
    f >> r;
    switch(r){
    case RFROG:
      frogs++;
      grid[i] = FROG;
      break;
    case RNOFROG:
      grid[i] = NOFROG;
      break;
    case 'x':
      grid[i] = -1;
      break;

    default:
      std::cerr << "ERROR: unexpected character " << r << " in file "
                << filename << std::endl;
      free(grid);
      exit(EXIT_FAILURE);
      break;
    }
  }

  if(frogs != max_frogs){
    std::cerr << "ERROR: expected " << max_frogs << " frogs but read " << frogs
              << std::endl;
    free(grid);
    exit(EXIT_FAILURE);
  }

  f.close();
  
  return new Pond(l, c, frogs, grid);
}