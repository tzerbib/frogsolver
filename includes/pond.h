#pragma once

#include <cstddef>

#define EMPTY   -1
#define NOFROG   0
#define FROG     1
#define RNOFROG '0'
#define RFROG   '1'

class Pond{
  private:
    size_t l;
    size_t c;
    size_t nb_frogs;
    char* grid;

  public:
    Pond(size_t, size_t, size_t, char*);
    Pond(const Pond&);
    Pond& operator=(const Pond&);
    ~Pond();
    size_t get_l() const;
    size_t get_c() const;
    size_t getNbFrogs() const;
    size_t setNbFrogs(size_t n);
    char* getGrid() const;
    void copyGrid(char*);
    void printGrid() const;
    void addFrog(size_t, size_t);
    void delFrog(size_t, size_t);
};
