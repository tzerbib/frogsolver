MODE     ?= DEBUG
CXX       = g++
BIN       = frogsolver
OPATH     = obj/
CPPPATH   = src/
BPATH     = bin/
DEPPATH   = deps/
HPATH     = includes/
INCLUDE   = -I$(HPATH)
CPPFILES  = $(wildcard $(CPPPATH)*.cpp)
OFILES    = $(patsubst $(CPPPATH)%.cpp, $(OPATH)%.o, $(CPPFILES))
DEPS      = $(patsubst $(CPPPATH)%.cpp, $(DEPPATH)%.d, $(CPPFILES))
AUTHOR    = Timothee_ZERBIB
VERSION   = v1_0
TARNAME   = $(BIN)_$(VERSION)

ECHO      = /bin/echo
ECHOFLAGS = -e


# Color definition
COLOR_DEFAULT  = \e[39m
COLOR_BLACK    = \e[30m 
COLOR_RED      = \e[31m
COLOR_GREEN    = \e[32m
COLOR_YELLOW   = \e[33m
COLOR_BLUE     = \e[34m
COLOR_MAGENTA  = \e[35m
COLOR_CYAN     = \e[36m


# Checking MODE maccro
ifeq ($(MODE),DEBUG)
	CXXFLAGS += -Wall -Wextra -g -Wimplicit-fallthrough=1
else ifeq ($(MODE), RELEASE)
	CXXFLAGS += -O3
else
erreur:
	@$(ECHO) $(ECHOFLAGS) "$(COLOR_RED)ERROR: Invalid compilation mode!"
	@$(ECHO) $(ECHOFLAGS) "Valid modes are:"
	@$(ECHO) $(ECHOFLAGS) "  - DEBUG"
	@$(ECHO) $(ECHOFLAGS) "  - RELEASE$(COLOR_DEFAULT)"
endif

LDFLAGS  += $(CXXFLAGS)

# OpenMP flags
OMPFLAGS += -fopenmp
CXXFLAGS += $(OMPFLAGS)
LDFLAGS  += $(OMPFLAGS)


# Possibility to use command "make run <file>"
SUPPORTED_COMMANDS := run
SUPPORTS_MAKE_ARGS := $(findstring $(firstword $(MAKECMDGOALS)), $(SUPPORTED_COMMANDS))
ifneq "$(SUPPORTS_MAKE_ARGS)" ""
  COMMAND_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(COMMAND_ARGS):;@:)
endif


.PHONY: help clean mrproper run dist


# main
$(BPATH)$(BIN): $(OFILES)
	@mkdir -p $(BPATH)
	@$(ECHO) $(ECHOFLAGS) "$(COLOR_CYAN)---- LINKING $@ ----$(COLOR_DEFAULT)"
	@$(CXX) -o $@ $^ $(LDFLAGS)
	@$(ECHO) $(ECHOFLAGS) "$(COLOR_GREEN)----  COMPILATION OF $@ COMPLETED $(COLOR_DEFAULT)"



# Generating dependencies
$(DEPPATH)%.d: $(CPPPATH)%.cpp
	@mkdir -p $(DEPPATH)
	@$(ECHO) $(ECHOFLAGS) "$(COLOR_MAGENTA)---- GENERATING DEPENDENCIES FOR $< ----$(COLOR_DEFAULT)"
	@$(ECHO) $(ECHOFLAGS) -n "$(OPATH)" > $@
	@$(CC) $(CXXFLAGS) $(INCLUDE) -MM $< >> $@


# Include generated dependencies
include $(DEPS)


# Compiling dependencies
$(OPATH)%.o: $(CPPPATH)%.cpp
	@mkdir -p $(OPATH)
	@$(ECHO) $(ECHOFLAGS) "$(COLOR_YELLOW)---- COMPILING $@ ----$(COLOR_DEFAULT)"
	@$(CC) $(CXXFLAGS) -c $< $(INCLUDE) -o $@



# Create a tarball containing all necessary files to reproduce results
dist: Makefile $(CPPPATH) $(HPATH)
	@mkdir $(TARNAME)
	@cp -r $^ $(TARNAME)
	@tar zcvf $(TARNAME).tar.gz $(TARNAME)
	@rm -rf $(TARNAME)


run: $(BPATH)$(BIN)
	@$< $(COMMAND_ARGS)

# Remove compilation artifacts
clean:
	@rm -vrf $(OPATH) $(DEPPATH)
	@rm -f *.tar.xz


# Remove binary and tarball
mrproper: clean
	@rm -vrf $(BPATH) $(TARNAME).tar.gz

help:
	@$(ECHO) $(ECHOFLAGS) "Usage: make [<target>]"
	@$(ECHO) $(ECHOFLAGS) "\nAvailable targets are:"
	@$(ECHO) $(ECHOFLAGS) "  clean                   Removes most of the files created during compilation."
	@$(ECHO) $(ECHOFLAGS) "  mrproper                Get a complete clean repository."
	@$(ECHO) $(ECHOFLAGS) "  dist                    Create a tarball named '$(TARNAME).tar.gz' containing core files in order to distribute the project."
	@$(ECHO) $(ECHOFLAGS) "  help                    Display this information message."
	@$(ECHO) $(ECHOFLAGS) "  run <filename>          Run the program on the given input file."
